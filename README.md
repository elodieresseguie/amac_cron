This explains the crontab protocol for AMAC radiation testing.
Based on: https://www.raspberrypi.org/documentation/linux/usage/cron.md

To open crontab, type: crontab -e
This will show the list of commands running

Parsing crontab commands

```
# * * * * *  command to execute
# ┬ ┬ ┬ ┬ ┬
# │ │ │ │ │
# │ │ │ │ │
# │ │ │ │ └───── day of week (0 - 7) (0 to 6 are Sunday to Saturday, or use names; 7 is Sunday, the same as 0)
# │ │ │ └────────── month (1 - 12)
# │ │ └─────────────── day of month (1 - 31)
# │ └──────────────────── hour (0 - 23)
# └───────────────────────── min (0 - 59)
```

We can also execute commands at reboot by replacing all the stars with `@reboot`
ie: `@reboot python myRebootProtocol.py`

All crontab commands are copied for reference in crontab.conf.

One thing that is important to note is that because multiple cron jobs could run at the same time, we need to protect the scripts by run such that only one is run at a time.
This is done using LockFile.c and its executable LockFile.

In crontab, scripts are executed through LockFile.
ie: `* * * * * LockFile test`
You do not need to specify the .sh of the script for testScript

if you make changes to the LockFile.c file, you need to compile it. This is done by running in the cron folder:
source make.sh

All scripts run by crontab are fun in scripts folder.
- gitTransfer.sh : moves all text files into the git repository for the AMAC radiation data and pushes the text files to git with a dated commit message

For the gitTransfer script to work, the git repository needs to be cloned using ssh and a public key needs to be generated and added to the amacradiation list of public keys on bitbucket.org
For more information, read : https://bitbucket.org/account/user/amacradiation/ssh-keys/
#!/bin/bash
gitRepo="/home/HEP/AMAC/data/amac_radiation_data_test/"
cd $gitRepo
rm -f .git/index.lock
git pull
git add AMAC_*
now_daily=$(date +%F)
git commit -m "data from $now_daily"
git push

#!/bin/bash

piName=$1

piDir="/home/pi/AMAC_radiation/data/*"

datapath='/home/HEP/AMAC/data/amac_radiation_data_test/'

piIpAddress=`ping -n 1 $piName | grep "statistics" | sed "s/Ping statistics for //" | sed "s/://"`

date
echo "Connecting to $piName with ip address: $piIpAddress"

# use regex to check for a valid and properly formatted ip address, otherwise don't try to connect!
if [[ $piIpAddress =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
  rsync -r -e ssh pi@${piIpAddress}:$piDir $datapath --info=progress2 --exclude=".git/" --exclude=".AppleDouble/"
fi

echo "end"

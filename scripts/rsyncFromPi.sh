#!/bin/bash

piDir="/home/pi/AMAC_radiation/data/amac_radiation_data_test"

datapath='/home/HEP/AMAC/data/'

rsync -r -e ssh hep_pi3b_Dreamwire:$piDir $datapath --progress --exclude=".git/" --exclude=".AppleDouble/"


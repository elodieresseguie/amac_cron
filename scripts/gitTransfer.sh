#!/bin/bash
datapath='/home/pi/AMAC_radiation/data/*'
gitRepo="/home/pi/AMAC_radiation/git_data/amac_radiation_data_test/"
cd $gitRepo
git pull
rsync -r $datapath $gitRepo --progress --exclude=".git/" --exclude=".AppleDouble/"
git add AMAC_*
now_daily=$(date +%F)
git commit -m "data from $now_daily"
git push
